var Rx = require("rx");
var SECL_IST = require("../data/SEC_LIST.json");
function tulpe(val1, val2) { return { val1: val1, val2: val2 }; }
exports.tulpe = tulpe;
function tulpe3(val1, val2, val3) { return { val1: val1, val2: val2, val3: val3 }; }
exports.tulpe3 = tulpe3;
function tulpe4(val1, val2, val3, val4) { return { val1: val1, val2: val2, val3: val3, val4: val4 }; }
exports.tulpe4 = tulpe4;
function tulpe5(val1, val2, val3, val4, val5) { return { val1: val1, val2: val2, val3: val3, val4: val4, val5: val5 }; }
exports.tulpe5 = tulpe5;
function getSecurity(secName) {
    return { name: secName, lotSize: SECL_IST[secName] };
}
exports.getSecurity = getSecurity;
function getEnvVar(name) {
    return (process.env[name] ||
        process.env["npm_config_" + name] ||
        process.env["npm_package_config_" + name] || "").replace(/[\n\t\r\s]/g, "");
}
exports.getEnvVar = getEnvVar;
function createObserver(label) {
    return Rx.Observer.create(function (x) {
        console.log(label + ' Next: ', x);
    }, function (err) {
        console.log(label + ' Error: ' + err);
    }, function () {
        console.log(label + ' Completed');
    });
}
exports.createObserver = createObserver;
