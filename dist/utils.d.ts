import * as Rx from "rx";
import * as ts from "./types";
export interface ITulpe<T1, T2> {
    val1: T1;
    val2: T2;
}
export interface ITulpe3<T1, T2, T3> {
    val1: T1;
    val2: T2;
    val3: T3;
}
export interface ITulpe4<T1, T2, T3, T4> {
    val1: T1;
    val2: T2;
    val3: T3;
    val4: T4;
}
export interface ITulpe5<T1, T2, T3, T4, T5> {
    val1: T1;
    val2: T2;
    val3: T3;
    val4: T4;
    val5: T5;
}
export declare function tulpe<T1, T2>(val1: T1, val2: T2): ITulpe<T1, T2>;
export declare function tulpe3<T1, T2, T3>(val1: T1, val2: T2, val3: T3): ITulpe3<T1, T2, T3>;
export declare function tulpe4<T1, T2, T3, T4>(val1: T1, val2: T2, val3: T3, val4: T4): ITulpe4<T1, T2, T3, T4>;
export declare function tulpe5<T1, T2, T3, T4, T5>(val1: T1, val2: T2, val3: T3, val4: T4, val5: T5): ITulpe5<T1, T2, T3, T4, T5>;
export interface ISecurity {
    name: string;
    lotSize: number;
}
export declare function getSecurity(secName: string): ISecurity;
export declare function getEnvVar(name: string): string;
export declare function createObserver(label: string): Rx.Observer<any>;
export declare type ICmd = ts.ICmd;
export declare type IEnty = ts.IEnty;
export declare type INotif<T> = ts.INotif<T>;
export declare type ICmdTrade = ts.ICmdTrade;
export declare type IPortfolioPosition = ts.IPortfolioPosition;
export declare type INotifPortfolioChanged = ts.INotifPortfolioChanged;
export declare type INotifSilverSurferData = ts.INotifSilverSurferData;
export declare type IQuote = ts.IQuote;
export declare type INotifRequest<T> = ts.INotifRequest<T>;
export declare type INotifRequestPortfolioQuery = ts.INotifRequestPortfolioQuery;
