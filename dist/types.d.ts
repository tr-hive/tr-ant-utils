export interface IEnty {
    key: string;
    issuer: string;
    date: string;
}
export interface INotif<T> extends IEnty {
    execRef?: string;
    data: T;
    type: string;
}
export interface ICmdTrade {
    ticket: string;
    oper: string;
    quantity: number;
}
export interface ICmd extends IEnty {
    portfolio: string;
    trade: ICmdTrade;
    notif?: INotif<any>;
    reason: string;
}
export interface IPortfolioPosition {
    ticket: string;
    quantity: number;
    value: number;
}
export interface INotifPortfolioChanged {
    portfolio: string;
    value: number;
    cmd: ICmd;
    positions: IPortfolioPosition[];
}
export interface INotifSilverSurferData {
    ticket: string;
    oper: string;
    stop: number;
    force: number;
}
export interface IQuote {
    ticket: string;
    latestPrice: number;
    bid: number;
    ask: number;
}
export interface INotifRequest<Q> {
    type: string;
    query: Q;
}
export interface INotifRequestPortfolioQuery {
    portfolio: string;
}
