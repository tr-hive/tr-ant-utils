/// <reference path="../typings/tsd.d.ts"/>

import chai = require('chai'); 
var lib = require('../dist/rx');
var expect = chai.expect;
import Rx = require("rx");

interface  IType {t: string, p : number, l : string};

describe("test groupByWithLatest",  () => {

	it("emit values: primary first then secondary",  (done) => {
		
		var ps =  Rx.Observable.create<IType>(observer => {
			setTimeout(() => observer.onNext({t : "A", p : 5, l : "p"}), 50);
			setTimeout(() => observer.onNext({t : "A", p : 10, l : "p"}), 100);
			setTimeout(() => observer.onCompleted(), 180);
		}).publish();
				
		var ss =  Rx.Observable.create<IType>(observer => {
			setTimeout(() => observer.onNext({t : "A", p : 12, l : "s"}), 120);
		}).publish();

		ps.connect();
		ss.connect();
				
		setTimeout(() => {			 
			lib.groupByWithLatest(ps, ss, p => p.t, (s, k) => s.t == k)
			.subscribe(val => console.log(val), done, done) 
		}, 10);
	
	})
	
	it("emit values: secondary first then primary",  (done) => {
		
		var ps =  Rx.Observable.create<IType>(observer => {
			setTimeout(() => observer.onNext({t : "A", p : 7, l : "p"}), 70);
			setTimeout(() => observer.onCompleted(), 180);
		}).publish();
				
		var ss =  Rx.Observable.create<IType>(observer => {
			setTimeout(() => observer.onNext({t : "A", p : 4, l : "s"}), 40);
		}).publish();
		
		ps.connect();
		ss.connect();
		
		setTimeout(() => {
			lib.groupByWithLatest(ps, ss, p => p.t, (s, k) => s.t == k)
			.subscribe(val => console.log(val), done, done);
		}, 10);	
	})
	
	it.only("emit values: primary and the reset stream",  (done) => {
		
		var ps =  Rx.Observable.create<IType>(observer => {
			setTimeout(() => observer.onNext({t : "A", p : 7, l : "p"}), 70);
			//setTimeout(() => observer.onCompleted(), 180);
		}).share()
		
		var ss = Rx.Observable.never();
				
		var rs =  Rx.Observable.timer(100);
		
		setTimeout(() => {
			lib.groupByWithLatest(ps, ss, p => p.t, (s, k) => s.t == k, rs)
			.subscribe(val => console.log(val), done, done);
		}, 10);	
	})		
		
		
}) 
