import * as Rx from "rx"
import  * as ts from "./types"  

const SECL_IST = require("../data/SEC_LIST.json");
	
export interface ITulpe<T1, T2> {
	val1: T1
	val2: T2
}

export interface ITulpe3<T1, T2, T3> {
	val1: T1
	val2: T2
	val3: T3
}

export interface ITulpe4<T1, T2, T3, T4> {
	val1: T1
	val2: T2
	val3: T3
	val4: T4
}

export interface ITulpe5<T1, T2, T3, T4, T5> {
	val1: T1
	val2: T2
	val3: T3
	val4: T4
	val5: T5
}

export function tulpe<T1, T2>(val1: T1, val2: T2) : ITulpe<T1, T2> {return {val1 : val1, val2: val2}}
export function tulpe3<T1, T2, T3>(val1: T1, val2: T2, val3: T3) : ITulpe3<T1, T2, T3> {return {val1 : val1, val2: val2, val3: val3}}
export function tulpe4<T1, T2, T3, T4>(val1: T1, val2: T2, val3: T3, val4: T4) : ITulpe4<T1, T2, T3, T4> {return {val1 : val1, val2: val2, val3: val3, val4: val4}}
export function tulpe5<T1, T2, T3, T4, T5>(val1: T1, val2: T2, val3: T3, val4: T4, val5: T5) : ITulpe5<T1, T2, T3, T4, T5> {return {val1 : val1, val2: val2, val3: val3, val4: val4, val5: val5}}

export interface ISecurity {
	name: string
	lotSize: number	
}

export function getSecurity(secName: string) : ISecurity {
	return {name : secName, lotSize: SECL_IST[secName]};
}

export function getEnvVar(name: string) : string {
	return (process.env[name] || 
	process.env["npm_config_" + name] || 
	process.env["npm_package_config_" + name] || "").replace(/[\n\t\r\s]/g,"");	
}

export function createObserver(label: string) {
		
		return Rx.Observer.create(
				function (x: any) {
						console.log(label + ' Next: ', x);      
				},
				function (err : any) {
						console.log(label + ' Error: ' + err);   
				},
				function () {
						console.log(label + ' Completed');   
				}
		);  
}



export type ICmd = ts.ICmd;
export type IEnty = ts.IEnty;
export type INotif<T> = ts.INotif<T>;
export type ICmdTrade = ts.ICmdTrade;
export type IPortfolioPosition = ts.IPortfolioPosition;
export type INotifPortfolioChanged = ts.INotifPortfolioChanged;
export type INotifSilverSurferData =  ts.INotifSilverSurferData;
export type IQuote = ts.IQuote;
export type INotifRequest<T> = ts.INotifRequest<T>;
export type INotifRequestPortfolioQuery = ts.INotifRequestPortfolioQuery;
