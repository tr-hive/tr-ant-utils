//Sequency notifictaion => commander => executor


/**
* Base entity for all others
*/
export interface IEnty {
    /**
        * Unique identifier
        */
    key: string

    /**
        * name of the issuer
        */
    issuer: string

    /**
        * Date time of the operation in ISO format
        */
    date: string
}

/**
    * Notification message from notifier to commander
    */
export interface INotif<T> extends IEnty {
    
    /**
        * Previous execution reference which activate this notification, if any
        */
    execRef? : string

    /**
        * Arbitrary notification data, same type for one source
        */
    data : T
    
    /**
        * String representattion of T type
        */
    type: string 
}

export interface ICmdTrade {
    /**
        * Instrument ticker
        */
    ticket : string

    /**
        * Operation - buy | sell
        */
    oper : string

    quantity: number

}

/**
    * Command message from commander to executor
    */
export interface ICmd extends IEnty {

    /**
        * Name of the portfolio
        */
    portfolio: string

    /**
        * Trade data to execute
        */
    trade: ICmdTrade

    /**
        * Notification which activate commander
        */
    notif? : INotif<any>

    /**
        * Reson message for the command
        */
    reason: string
}

///
export interface IPortfolioPosition {
    ticket: string
    quantity: number
    value: number
}

export interface INotifPortfolioChanged {
    portfolio: string
    value: number
    /**
        * Command which changed portfolio
        */
    cmd : ICmd
    positions: IPortfolioPosition[]
}
    
export interface INotifSilverSurferData {
    ticket: string
    /**
        * buy | sell
        */
    oper: string
    stop: number
    force: number

}

//
export interface IQuote {
    ticket: string
    latestPrice: number
    bid: number
    ask: number
}    

export interface INotifRequest<Q> {
    type: string,
    query : Q 
}

export interface INotifRequestPortfolioQuery {
    portfolio: string
}

